from django.contrib import admin
from .models import Act, Box, BoxTransport, Device, DeviceImages, Place, Receipt, Statement, Logs, AuthUser

# Register your models here.
@admin.register(Act)
class ActAdmin(admin.ModelAdmin):
    list_display = ("act_id", "act_date", "type")
    search_fields = ("act_date",)
    list_filter = ("type",)
@admin.register(AuthUser)
class AuthUserAdmin(admin.ModelAdmin):
    list_display = ("username", "first_name")
@admin.register(Box)
class BoxAdmin(admin.ModelAdmin):
    list_display = ("id", "box_id", "box_type", "package_length", "package_width", "package_height",)
    ordering = ("id",)
    search_fields = ("box_id",)
@admin.register(Receipt)
class ReceiptAdmin(admin.ModelAdmin):
    list_display = ("receipt_id", "receipt_date")
    search_fields = ("receipt_id__startswith", "receipt_date")
@admin.register(Statement)
class StatementAdmin(admin.ModelAdmin):
    search_fields = ("device__device_id__startswith",)
    list_display = ("device_id","get_name" , "repair_date_begin", "work", "get_birthplace", "shelf")
    ordering = ("repair_date_begin", "device_id")
    list_filter = ("work", "statement")
    exclude = [ 'diagnostics_group']

    def get_name(self, obj):
        return obj.device.name

    get_name.admin_order_field = 'device'  # Allows column order sorting
    get_name.short_description = 'Наименование'  # Renames column head

    def get_birthplace(self, obj):
        return obj.device.birthplace

    get_birthplace.admin_order_field = 'device'  # Allows column order sorting
    get_birthplace.short_description = 'Месторождение'  # Renames column head

@admin.register(Device)
class DeviceAdmin(admin.ModelAdmin):
    search_fields = ("device_id__startswith", )
    list_display = ("device_id", "name", "denomination", "type")
    ordering = ("device_id", "denomination",)

@admin.register(Logs)
class LogsAdmin(admin.ModelAdmin):
    search_fields = ("time", "device_id", "comment" )
    list_display = ("time", "device_id", "comment",)
    ordering = ("time",)
    def format_date(self, obj):
        return obj.date.strftime('%Y-%m-%d %H:%M')
@admin.register(DeviceImages)
class DeviceImagesAdmin(admin.ModelAdmin):
    search_fields = ("image",)
    list_display = ("image","device")
#admin.site.register(DeviceData)
#admin.site.register(Repair)
#admin.site.register(Place)

