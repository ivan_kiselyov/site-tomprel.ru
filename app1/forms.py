from django import forms
from .models import Device, Receipt, Box, box_choice, type_choice, Statement, work_choice, doc_choice
from datetime import datetime


def year_choices():
    YEAR_CHOICES = []
    for i in range(10):
        YEAR_CHOICES.append(str(datetime.now().year-i))
    return YEAR_CHOICES
    MONTHS_CHOICES = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"]

def box_choices():
    boxes = list(Box.objects.all().order_by('-id'))

    boxes_lists_list = []
    for i in range(0, len(boxes)):
        boxes_list = []
        box = "(" + str(boxes[i].id) + ") "
        box+= "Дата поступления:"+str(boxes[i].arrive_date)+", "
        if boxes[i].box_id != "" and boxes[i].box_id is not None:
            box += "Номер ящика:" + str(boxes[i].box_id) + ", "
        box += "Габариты:" + str(boxes[i].package_length) + "*" + str(boxes[i].package_width) + "*" + str(
            boxes[i].package_height)
        boxes_list.append(box)
        boxes_list.append(box)
        boxes_lists_list.append(boxes_list)
    t_boxes_lists_list = tuple(boxes_lists_list)
    return t_boxes_lists_list

def repair_choices():
    statements = list(Statement.objects.filter(statement="В процессе").order_by('customer', '-statement_id'))
    print("repair_choices")
    print(statements)

    statements_lists_list = []
    for i in range(0, len(statements)):
        statements_list = []
        statement = str(statements[i].device.id) + ") Заказчик:" + str(statements[i].customer) + " Заводской номер:" + (statements[i].device.device_id) + " Дата прибытия:" + str(statements[i].repair_date_begin)
        print(statement)
        statements_list.append(statement)
        statements_list.append(statement)
        statements_lists_list.append(statements_list)
    t_statements_lists_list = tuple(statements_lists_list)
    print(t_statements_lists_list)
    return t_statements_lists_list

def diag_choices():
    statements = list(Statement.objects.filter(statement="В процессе").order_by('customer', '-statement_id'))
    print("diag_choices")
    print(statements)

    statements_lists_list = []
    for i in range(0, len(statements)):
        statements_list = []
        statement = str(statements[i].device.id) + ") Заказчик:" + str(statements[i].customer) + " Заводской номер:" + (statements[i].device.device_id) + " Дата прибытия:" + str(statements[i].repair_date_begin)

        print(statement)
        statements_list.append(statement)
        statements_list.append(statement)
        statements_lists_list.append(statements_list)
    t_statements_lists_list = tuple(statements_lists_list)
    print(t_statements_lists_list)
    return t_statements_lists_list

class BoxChoiceForm(forms.Form):
    box_id = forms.CharField(max_length=30, label="Номер ящика", required = False)
    box_type = forms.ChoiceField(choices=box_choice, label="Тип упаковки", required = False)
    length = forms.CharField(max_length=10, label="Длина ящика (мм)", required = False)
    width = forms.CharField(max_length=10, label="Ширина ящика (мм)", required = False)
    height = forms.CharField(max_length=10, label="Высота ящика (мм)", required = False)
    weight = forms.CharField(max_length=10, label="Масса (кг)", required = False)

    # time = forms.DateField(label='Дата прибытия', initial=datetime.now())

class DeviceChoiceForm(forms.Form):
    repair_date_begin = forms.DateField(label='Дата прибытия', widget=forms.SelectDateWidget(years=year_choices()),
                           initial=datetime.now())
    device_id = forms.CharField(max_length=30, label="Заводской номер устройства")
    customer = forms.CharField(label='Заказчик', max_length=50)
    birthplace = forms.CharField(label='Место установки, эксплуатации', max_length=60, required=False)
    box = forms.MultipleChoiceField(choices=box_choices, label="Ящик", required=False)
    receipt = forms.ModelMultipleChoiceField(queryset=Receipt.objects.all(), label="Квитанция", required = False)
    shelf = forms.IntegerField(label="Ячейка")
    work = forms.MultipleChoiceField(choices=work_choice, widget=forms.CheckboxSelectMultiple, label="Тип работ", required = False)
    doc = forms.MultipleChoiceField(choices=doc_choice, widget=forms.CheckboxSelectMultiple, label="Комплектация документов", required = False)
    name = forms.CharField(label='Наименование оборудования', max_length=70, required = False)
    type = forms.MultipleChoiceField(label='Тип оборудования', required = False, choices=type_choice)
    denomination = forms.CharField(label='Вид оборудования', max_length=30, required = False)
    brand = forms.CharField(label='Производитель оборудования', max_length=30, required = False)
    photos = forms.CharField(label = 'Фотографии', max_length = 500000)



class ReceiptChoiceForm(forms.Form):
    receipt_id = forms.CharField(max_length=50, label="Номер квитанции")
    receipt_date = forms.DateField(label='Дата квитанции', widget=forms.SelectDateWidget(years=year_choices()),)
    image = forms.ImageField(label="Фотография квитанции", required=False)
    # class Meta:
    #     model = Receipt
    #     fields = ('receipt_id', 'image')

class DiagnosticsForm(forms.Form):
    device_id = forms.MultipleChoiceField(choices=diag_choices(), label='Устройство (выбирать несколько через "ctrl")')
    diagnostics_result = forms.CharField(max_length=500, label="По результатам диагностики  выявлены неисправности:", widget=forms.Textarea, initial="неисправность прибора, выработка ресурса")
    diagnostics_end_date = forms.DateField(label='Дата окончания диагностики', widget=forms.SelectDateWidget(years=year_choices()),
                                  initial=datetime.now())

class RepairForm(forms.Form):
    device_id = forms.MultipleChoiceField(choices=repair_choices, label='Устройство (выбирать несколько через "ctrl")')
    repair_result = forms.CharField(max_length=500, label="Проведены работы:", widget=forms.Textarea, initial="ремонт прибора, замена ресурса")
    repair_date_end = forms.DateField(label='Дата окончания диагностики',
                                           widget=forms.SelectDateWidget(years=year_choices()),
                                           initial=datetime.now())


# model_choice = forms.ModelChoiceField(
#     queryset=Device.objects.all(),
#     initial=0,
#     label="Заводской номер устройства",
# )
