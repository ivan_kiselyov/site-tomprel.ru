# Generated by Django 3.1 on 2020-09-30 14:50

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app1', '0009_delete_actpage'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Repair',
        ),
        migrations.AlterModelOptions(
            name='statement',
            options={'managed': False, 'verbose_name': 'Статус', 'verbose_name_plural': 'Статусы'},
        ),
    ]
