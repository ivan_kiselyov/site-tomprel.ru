from django.db import models
import datetime, re
from multiselectfield import MultiSelectField

YEAR_CHOICES = []
for r in range(1970, (datetime.datetime.now().year+1)):
    YEAR_CHOICES.append((r,r))

type_choice = (
    ('', '-'),
    ('Контроллер', 'Контроллер'),
    ('Преобразователь температуры', 'Преобразователь температуры'),
    ('Преобразователь давления', 'Преобразователь давления'),
    ('Преобразователь влажности', 'Преобразователь влажности'),
    ('Преобразователь длины', 'Преобразователь длины'),
    ('Преобразователь виброскорости', 'Преобразователь виброскорости'),
    ('Источник бесперебойного питания', 'Источник бесперебойного питания'),
    ('Блок питания', 'Блок питания'),
    ('Сигнализатор', 'Сигнализатор'),
    ('Газосигнализатор', 'Газосигнализатор'),
    ('Коммутатор', 'Коммутатор'),
    ('Расходомер', 'Расходомер'),
    ('Барьер', 'Барьер'),
    ('Видеокамера', 'Видеокамера'),
    ('Нагреватель', 'Нагреватель')
)

doc_choice = (("акт неисправности", 'Акт неисправности'),
                #("fault_report", 'Акт неисправности'),
               ("свидетельство о поверке", 'Свидетельство о поверке'),
              #("verification_certificate", 'Свидетельство о поверке'),
               ("паспорт", 'Паспорт'),
              #("passport", 'Паспорт'),
               ("руководство по эксплуатации", 'Руководство по эксплуатации')
              #("user_manual", 'Руководство по эксплуатации')
              )

work_choice = (("диагностика", 'Диагностика'),
               ("ремонт", 'Ремонт'),
               ("поверка", 'Поверка'),
              )


stat_choice = (("В процессе", 'В процессе'),
              #("in_progress", 'В процессе'),
               # ("Согласование", 'Согласование'),
              #("agreement", 'Согласование'),
               ("Завершено", 'Завершено'))
              #("completed", 'Завершено'))

box_choice = (("Ящик", 'Ящик'),
               ("Коробка", 'Коробка'),
               ("Мешок", 'Мешок'))

act_choice = ( (1, 'Акт приёма-передачи'),
               (2, 'Акт выполненных работ'),
               (3, 'Акт неисправности'),
               (4, 'Акт списания'),
               (5, 'Акт ремонта'),
             )



class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.IntegerField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=254)
    is_staff = models.IntegerField()
    is_active = models.IntegerField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class Box(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    box_id = models.CharField("Номер ящика", max_length=30, blank=True, null=True)
    box_type = models.CharField("Упаковка", max_length=50, blank=True, null=True, choices=box_choice)
    package_length = models.IntegerField("Длина упаковки", blank=True, null=True)
    package_width = models.IntegerField("Ширина упаковки", blank=True, null=True)
    package_height = models.IntegerField("Высота упаковки", blank=True, null=True)
    package_weight = models.FloatField("Масса", blank=True, null=True)
    arrive_date = models.DateField("Дата поступления", blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'box'
        verbose_name = "Ящик"
        verbose_name_plural = "Ящики"




class BoxTransport(models.Model):
    box_transport_id = models.AutoField(primary_key=True)
    box = models.ForeignKey(Box, models.SET_NULL, blank=True, null=True)
    sent_date = models.DateField(blank=True, null=True)
    sent_place = models.CharField(max_length=30, blank=True, null=True)
    arrive_place = models.CharField(max_length=30, blank=True, null=True)
    transport_name = models.CharField(max_length=30, blank=True, null=True)
    transport_type = models.CharField(max_length=30, blank=True, null=True)
    transport_denomination = models.CharField(max_length=30, blank=True, null=True)
    transport_price = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'box_transport'
        verbose_name = "Перемещение ящика"
        verbose_name_plural = "Перемещения ящика"

class Device(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    device_id = models.CharField('Заводской номер', blank=True, null=True, max_length=50)
    issue_year = models.IntegerField('Год выпуска', choices=YEAR_CHOICES, default=datetime.datetime.now().year)
    name = models.CharField('Наименование оборудования', max_length=70, blank=True, null=True)
    type = models.CharField('Тип оборудования', max_length=50, blank=True, null=True, choices=type_choice)
    denomination = models.CharField('Вид оборудования', max_length=30, blank=True, null=True)
    brand = models.CharField('Производитель оборудования', max_length=30, blank=True, null=True)
    #model = models.CharField('Модель оборудования', max_length=30, blank=True, null=True)
    birthplace = models.CharField('Место установки, эксплуатации', max_length=60, blank=True, null=True)


    class Meta:
        managed = False
        db_table = 'device'
        verbose_name = "Устройство"
        verbose_name_plural = "Устройства"

class DeviceImages(models.Model):
    image = models.TextField(blank=True, null=True)
    device = models.ForeignKey(Device, models.DO_NOTHING, db_column='device', related_name='photos')

    class Meta:
        managed = False
        db_table = 'device_images'
        verbose_name = "Фото устройств"
        verbose_name_plural = "Фото устройств"

class Place(models.Model):
    place_id = models.CharField(primary_key=True, max_length=30)
    place_name = models.CharField(max_length=30, blank=True, null=True)
    place_denomination = models.CharField(max_length=30, blank=True, null=True)
    place_destination = models.CharField(max_length=30, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'place'
        verbose_name = "Место"
        verbose_name_plural = "Места"

class Receipt(models.Model):
    receipt_id = models.CharField('Номер квитанции', primary_key=True, max_length=30)
    receipt_date = models.DateField('Дата квитанции', blank=True, null=True)
    image = models.ImageField("Фотография", upload_to='media/', blank=True, null=True)


    class Meta:
        managed = False
        db_table = 'receipt'
        verbose_name = "Квитанция"
        verbose_name_plural = "Квитанции"



class Act(models.Model):
    act_id = models.AutoField('ID акта', primary_key=True)
    act_date = models.DateField('Дата акта', blank=True, null=True)
    type = models.IntegerField('Тип акта', choices=act_choice)
    image = models.ImageField(upload_to='static/')

    class Meta:
        managed = False
        db_table = 'act'
        ordering = ["act_date"]
        verbose_name = "Акт"
        verbose_name_plural = "Акты"

class DiagnosticsGroup(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'diagnostics_group'

class RepairGroup(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)

    class Meta:
        managed = False
        db_table = 'repair_group'


class Statement(models.Model):
    statement_id = models.AutoField(primary_key=True)
    work = MultiSelectField('Тип работ', max_length=30, blank=True, null=True, choices=work_choice)
    statement = models.CharField('Статус', max_length=30, blank=True, null=True, choices=stat_choice)
    doc = MultiSelectField(choices=doc_choice, max_choices=4, max_length=100, blank=True, null=True, verbose_name="Комплектность документов")
    customer = models.CharField(max_length=50, blank=True, null=True, verbose_name="Заказчик")
    box = models.ForeignKey(Box, models.SET_NULL, blank=True, null=True, verbose_name="Ящик")
    place = models.ForeignKey(Place, models.SET_NULL, blank=True, null=True, verbose_name="Местонахождение")
    device = models.ForeignKey(Device, models.SET_NULL, blank=True, null=True, verbose_name="Устройство")
    receipt = models.ForeignKey(Receipt, models.SET_NULL, blank=True, null=True, verbose_name="Квитанция")
    malfunction_desc = models.TextField('Причина поступления', blank=True, null=True)
    diagnostics_end_date = models.DateField('Дата окончания диагностики', blank=True, null=True)
    diagnostics_result = models.TextField('Результаты диагностики', max_length=500, blank=True, null=True)
    repair_date_begin = models.DateField('Дата поступления', blank=True, null=True)
    repair_date_end = models.DateField('Дата окончания работ',blank=True, null=True)
    repair_result = models.TextField('Результат работ', blank=True, null=True)
    repair_group = models.ForeignKey(RepairGroup, models.DO_NOTHING, db_column='repair_group', blank=True, null=True)
    shelf = models.IntegerField('Ячейка')
    customer_transl = models.CharField(max_length=50, blank=True, null=True)
    diagnostics_group = models.ForeignKey(DiagnosticsGroup, models.DO_NOTHING, db_column='diagnostics_group',
                                          blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'statement'
        verbose_name = "Статус"
        verbose_name_plural = "Статусы"


class Logs(models.Model):
    time = models.DateTimeField('Время', primary_key=True, auto_now=True,)
    comment = models.TextField('Комментарий', max_length=200, blank=True, null=True)
    device = models.ForeignKey(Device, models.SET_NULL, blank=True, null=True, verbose_name="Устройство")

    class Meta:
        managed = False
        db_table = 'logs'
        verbose_name = "Логи"
        verbose_name_plural = "Логи"