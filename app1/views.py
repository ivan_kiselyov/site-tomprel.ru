from datetime import date as datefunc
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect
from .models import Act, Device, Statement, BoxTransport, Box, Receipt, Place, DeviceImages, DiagnosticsGroup, RepairGroup
from django.views.generic.base import View
# Create your views here.

#Импорт форм
from .forms import BoxChoiceForm, DeviceChoiceForm, ReceiptChoiceForm, box_choices, DiagnosticsForm, \
    diag_choices, repair_choices, RepairForm

def transliterate(name): # перевод кириллического текста в транслит, также замена запрещенных в MySQL знаков
    # Словарь с заменами
    slovar = {'а': 'a', 'б': 'b', 'в': 'v', 'г': 'g', 'д': 'd', 'е': 'e', 'ё': 'e',
              'ж': 'zh', 'з': 'z', 'и': 'i', 'й': 'i', 'к': 'k', 'л': 'l', 'м': 'm', 'н': 'n',
              'о': 'o', 'п': 'p', 'р': 'r', 'с': 's', 'т': 't', 'у': 'u', 'ф': 'f', 'х': 'h',
              'ц': 'c', 'ч': 'cz', 'ш': 'sh', 'щ': 'scz', 'ъ': '', 'ы': 'y', 'ь': '', 'э': 'e',
              'ю': 'u', 'я': 'ja', 'А': 'A', 'Б': 'B', 'В': 'V', 'Г': 'G', 'Д': 'D', 'Е': 'E', 'Ё': 'E',
              'Ж': 'ZH', 'З': 'Z', 'И': 'I', 'Й': 'I', 'К': 'K', 'Л': 'L', 'М': 'M', 'Н': 'N',
              'О': 'O', 'П': 'P', 'Р': 'R', 'С': 'S', 'Т': 'T', 'У': 'U', 'Ф': 'F', 'Х': 'H',
              'Ц': 'C', 'Ч': 'CZ', 'Ш': 'SH', 'Щ': 'SCH', 'Ъ': '', 'Ы': 'y', 'Ь': '', 'Э': 'E',
              'Ю': 'U', 'Я': 'YA', ',': '', '?': '', ' ': '', '~': '', '!': '', '@': '', '#': '',
              '$': '', '%': '', '^': '', '&': '', '*': '', '(': '', ')': '', '-': '', '=': '', '+': '',
              ':': '', ';': '', '<': '', '>': '', '\'': '', '"': '', '\\': '', '/': '', '№': 'N',
              '[': '', ']': '', '{': '', '}': '', 'ґ': '', 'ї': '', 'є': '', 'Ґ': 'g', 'Ї': 'i',
              'Є': 'e', '—': '', '.':''}

    # Циклически заменяем все буквы в строке
    for key in slovar:
        name = name.replace(key, slovar[key])
    return name

def delete_com(name):
    slovar = {'[': '', ']': '', "'": ''}
    for key in slovar:
        name = name.replace(key, slovar[key])
    return name

def save_statements(statement_id=None, diagnostics_result=None, work=None, statement=None, doc=None, place=None,
                    device=None, receipt=None, malfunction_desc=None, repair_date_begin=None,
                    repair_date_end=None, repair_result=None, shelf=None, customer=None, customer_transl=None, box = None,diagnostics_group= None,
                    diagnostics_end_date = None,repair_group= None, repair_end_date=None):

    if statement_id is not None:
        if diagnostics_result is None:
            diagnostics_result = Statement.objects.get(statement_id=statement_id).diagnostics_result
        if work is None:
            work = Statement.objects.get(statement_id=statement_id).work
        if statement is None:
            statement = Statement.objects.get(statement_id=statement_id).statement
        if doc is None:
            doc = Statement.objects.get(statement_id=statement_id).doc
        if box is None:
            box = Statement.objects.get(statement_id=statement_id).box
        if customer is None:
            customer = Statement.objects.get(statement_id=statement_id).customer
        if customer_transl is None:
            customer_transl = Statement.objects.get(statement_id=statement_id).customer_transl
            place = Statement.objects.get(statement_id=statement_id).place
        if device is None:
            device = Statement.objects.get(statement_id=statement_id).device
        if receipt is None:
            receipt = Statement.objects.get(statement_id=statement_id).receipt
        if malfunction_desc is None:
            malfunction_desc = Statement.objects.get(statement_id=statement_id).malfunction_desc
        if repair_date_begin is None:
            repair_date_begin = Statement.objects.get(statement_id=statement_id).repair_date_begin
        if repair_result is None:
            repair_result = Statement.objects.get(statement_id=statement_id).repair_result
        if shelf is None:
            shelf = Statement.objects.get(statement_id=statement_id).shelf
        if diagnostics_group is None:
            diagnostics_group = Statement.objects.get(statement_id=statement_id).diagnostics_group
        if diagnostics_end_date is None:
            diagnostics_end_date = Statement.objects.get(statement_id=statement_id).diagnostics_end_date
        if repair_group is None:
            repair_group = Statement.objects.get(statement_id=statement_id).repair_group
        if repair_date_end is None:
            repair_date_end = Statement.objects.get(statement_id=statement_id).repair_date_end

        s = Statement(statement_id=int(statement_id), diagnostics_result=str(diagnostics_result), work=work, statement=statement,
                    doc=doc, place=place,
                    device=device,  receipt=receipt, malfunction_desc=malfunction_desc, repair_date_begin=repair_date_begin,
                    repair_date_end=repair_date_end, repair_result=repair_result, shelf=shelf, customer_transl=customer_transl, customer=customer, box = box, diagnostics_group=diagnostics_group,
                    diagnostics_end_date= diagnostics_end_date, repair_group  =  repair_group)
        s.save()


class DeviceView(LoginRequiredMixin, View):
    "Главная страница"


    def get(self, request):
        devices = Device.objects.all()
        device_choice_form = DeviceChoiceForm()
        return render(request, "devices/main_page.html", {"Devices": devices, 'device_choice_form': device_choice_form})

    def get_devices_list(request):
        statements = Statement.objects.all()
        devices = Device.objects.all()
        return render(request, "devices/all_devices.html", {"Devices": devices, "Statements": statements})

    def get_devices_list_button(self, *args, **kwargs):
        return redirect('deviceslist')


class LogoutButton(View):
    def post(self, *args, **kwargs):
        # Ваш код
        return redirect('/account/logout')


class ManagerInterface (LoginRequiredMixin, View):

    def get_new_device(request):
        # act = Act.objects.all()
        # devices = Device.objects.all()
        box_choice_form = BoxChoiceForm()
        receipt_choice_form = ReceiptChoiceForm()
        box_choices()  # обновляем список ящиков
        device_choice_form = DeviceChoiceForm()
        print(Box.objects.all())

        return render(request, "devices/new_device.html",
                      {'box_choice_form': box_choice_form, 'receipt_choice_form': receipt_choice_form,
                       'device_choice_form': device_choice_form, })


    def get_diagnostics(request):
        repair_choices()
        diag_choices()
        diagnostics_form = DiagnosticsForm()


        statements = Statement.objects.filter(statement="В процессе")



        return render(request, "devices/new_diagnostics.html", {'diagnostics_form': diagnostics_form})

        # return render(request, "devices/new_diagnostics.html", {'diagnostics_form': diagnostics_form, 'customers' :customers})

    def get_new_repair(request):
        # act = Act.objects.all()
        # devices = Device.objects.all()
        repair_choices()
        diag_choices()
        repair_form = RepairForm()

        return render(request, "devices/new_repair.html", {'repair_form': repair_form})
# def get_new_box_receipt(request):diagnostics
#     act = Act.objects.all()
#     devices = Device.objects.all()
    def get_act_list(request):
        statements = Statement.objects.all()
        print(statements)

        customers = []
        for stat in statements:
            if stat.customer not in customers:
                customers.append(stat.customer)
        print(customers)
        act_dates = []
        act_customers = []
        # boxes_in_statements = []
        boxes = Box.objects.all()
        i = 0
        for cl in customers:
            statements = Statement.objects.filter(statement="В процессе", customer=str(cl))
            dates_in_statement = []
            for stat in statements:
                print(stat.box)
                if (stat.repair_date_begin not in dates_in_statement) and (stat.repair_date_begin is not None):
                    dates_in_statement.append(stat.repair_date_begin) #для проверки внутри цикла
                    # boxes_in_statements.append(stat.box)
                    act_dates.append(stat.repair_date_begin)
                    act_customers.append(cl)
        print("Массив")
        print(act_dates)
        print(act_customers)
        acts = []
        for i in range(0, len(act_dates)):
            act = []
            act.append(act_customers[i])
            act.append(act_dates[i])
            act.append(transliterate(str(act_customers[i])+str(act_dates[i])))
            statements = Statement.objects.filter(customer=str(act_customers[i]))
            customer_transl = transliterate(act_customers[i])
            acts.append(act)
            for statement in statements:
                print("Алё")
                print(statement.statement_id)
                save_statements(statement.statement_id, customer_transl=customer_transl)

        print("Результат")
        print(acts)
        return render(request, "devices/act_list.html", {"acts": acts})

    def get_act(request, act):
        print("Результат:")
        print(act)
        day_str = act[-2:]
        print(day_str)
        act=act[0:-2]
        month_str = act[-2:]
        print(month_str)
        act = act[0:-2]
        year_str = act[-4:]
        print(year_str)
        act = act[0:-4]
        customer_transl = act
        date = datefunc(year=int(year_str), month=int(month_str), day=int(day_str))

        final_statements = []
        statements = Statement.objects.filter(customer_transl = customer_transl).order_by('device__name')
        print(statements)
        for state in statements:
            print(state.repair_date_begin)
            print(date)
            if state.repair_date_begin == date:
                final_statements.append(state)

        devices = []
        num=1

        date = statements[0].repair_date_begin  # 0 - Дата квитанции

        counts = []
        i=0
        # for state in final_statements:
        #     if state.device.type not in counts[i][0]:


        for state in final_statements:
            device = []

            device.append(num)  # 0 - Номер
            device.append(state.device.name)  # 1 - Наименование прибора
            device.append(delete_com(str(state.device.type))) # 2 - Тип прибора
            device.append("1") # 3 - Кол-во штук
            device.append(state.device.device_id) # 4 - Зав. номер устройства
            device.append(state.device.birthplace) # 5 - Месторождение
            try:
                device.append(state.receipt.receipt_id)  # 6 - номер квитанции
            except:
                device.append(None)
            try:
                device.append(state.receipt.receipt_date)  # 7 - дата согласования квитанции
            except:
                device.append(None)
            device.append(delete_com(str(state.work))) # 8 - тип работ
            device.append(state.doc) # 9 - комплектность документов
            try:
                device.append(state.box.box_type)# 10 - тип упаковки
            except:
                device.append(None)
            try:
                device.append(state.box.package_length)# 11 - длина упаковки
                device.append(state.box.package_width)# 12 - ширина упаковки
                device.append(state.box.package_height)# 13 - высота упаковки
            except:
                device.append(None)
                device.append(None)
                device.append(None)
            try:
                device.append(state.box.package_weight)  # 14 - вес упаковки
            except:
                device.append(None)
            device.append("Томск")  # 15 - местонахождение
            devices.append(device)
            num += 1
            print(devices)
        return render(request, "devices/act_page1.html", {"devices": devices, "date": date})

    def get_act_list_defects(request):
        statements = Statement.objects.all()

        customers = []
        for stat in statements:
            if stat.customer not in customers:
                customers.append(stat.customer)
        print(customers)
        act_id = []
        act_customers = []
        act_customers_transl = []
        act_dates = []
        # boxes_in_statements = []
        diag_groups_all = DiagnosticsGroup.objects.all()
        i = 0
        for cl in customers:
            statements = Statement.objects.filter(customer=str(cl))
            diag_groups = []
            for stat in statements:
                print(stat.diagnostics_group)
                for diag in diag_groups_all:
                    if (stat.diagnostics_group == diag) and (stat.diagnostics_group not in diag_groups) and (stat.diagnostics_group is not None):
                        act_id.append(stat.diagnostics_group.id)
                        act_dates.append(stat.diagnostics_end_date)
                        act_customers.append(cl)
                        act_customers_transl.append(stat.customer_transl)
                        diag_groups.append(stat.diagnostics_group)
        print("Массив")
        print(act_id)
        print(act_customers)
        acts = []
        for i in range(0, len(act_id)):
            act = []
            act.append(act_id[i])
            act.append(act_customers[i])
            act.append(act_dates[i])
            act.append(str(act_customers_transl[i])+str(act_dates[i]))
            acts.append(act)

        print("Результат")
        print(acts)
        return render(request, "devices/act_list2.html", {"acts": acts})

    def get_act_defects(request, act):
        print("Результат:")
        print(act)

        day_str = act[-2:]
        print(day_str)
        act = act[0:-3]
        month_str = act[-2:]
        print(month_str)
        act = act[0:-3]
        year_str = act[-4:]
        print(year_str)
        act = act[0:-4]

        date = datefunc(year=int(year_str), month=int(month_str), day=int(day_str))

        final_statements = []
        statements = Statement.objects.filter(diagnostics_end_date=datefunc(year=int(year_str), month=int(month_str), day=int(day_str)), customer_transl = act)
        print(statements)
        for state in statements:
            print(state.diagnostics_end_date)
            print(date)
            if state.diagnostics_end_date == date:
                final_statements.append(state)

        devices = []
        num = 1
        diag_res = final_statements[0].diagnostics_result
        date = statements[0].repair_date_begin  # 0 - Дата квитанции
        for state in final_statements:
            device = []

            device.append(num)  # 0 - Номер
            device.append(state.device.name)  # 1 - Наименование прибора
            device.append(delete_com(str(state.device.type)))  # 2 - Тип прибора
            device.append("1")  # 3 - Кол-во штук
            device.append(state.device.device_id)  # 4 - Зав. номер устройства
            device.append(state.device.birthplace)  # 5 - Месторождение
            devices.append(device)
            num += 1
            print(devices)
        return render(request, "devices/act_page2.html", {"devices": devices, "diag_res": diag_res, "date": date})

    def get_act_list_repair(request):
        statements = Statement.objects.all()

        customers = []
        for stat in statements:
            if stat.customer not in customers:
                customers.append(stat.customer)
        print(customers)
        act_id = []
        act_customers = []
        act_customers_transl = []
        act_dates = []
        # boxes_in_statements = []
        repair_groups_all = RepairGroup.objects.all()
        i = 0
        for cl in customers:
            statements = Statement.objects.filter(customer=str(cl))
            repair_groups = []
            for stat in statements:
                print(stat.diagnostics_group)
                for repair in repair_groups_all:
                    if (stat.repair_group == repair) and (stat.repair_group not in repair_groups) and (stat.repair_group is not None):
                        act_id.append(stat.repair_group.id)
                        act_dates.append(stat.repair_date_end)
                        act_customers.append(cl)
                        act_customers_transl.append(stat.customer_transl)
                        repair_groups.append(stat.diagnostics_group)
        print("Массив")
        print(act_id)
        print(act_customers)
        for group in act_id:
            states_for_save = Statement.objects.filter(repair_group = group)
            for stat in states_for_save:
                save_statements(stat.statement_id, statement = "Завершено")
        acts = []
        for i in range(0, len(act_id)):
            act = []
            act.append(act_id[i])
            act.append(act_customers[i])
            act.append(act_dates[i])
            act.append(str(act_customers_transl[i])+str(act_dates[i]))
            acts.append(act)

        print("Результат")
        print(acts)
        return render(request, "devices/act_list3.html", {"acts": acts})

    def get_act_repair(request, act):
        print("Результат:")
        print(act)

        day_str = act[-2:]
        print(day_str)
        act = act[0:-3]
        month_str = act[-2:]
        print(month_str)
        act = act[0:-3]
        year_str = act[-4:]
        print(year_str)
        act = act[0:-4]

        date = datefunc(year=int(year_str), month=int(month_str), day=int(day_str))

        final_statements = []
        statements = Statement.objects.filter(repair_date_end=datefunc(year=int(year_str), month=int(month_str), day=int(day_str)), customer_transl = act).order_by("device__birthplace","device__type")
        print(statements)
        for state in statements:
            print(state.repair_date_end)
            print(date)
            if state.repair_date_end == date:
                final_statements.append(state)

        devices = []
        num = 1
        diag_res = final_statements[0].diagnostics_result
        repair_res = final_statements[0].repair_result
        for state in final_statements:
            device = []

            device.append(num)  # 0 - Номер
            device.append(state.device.name)  # 1 - Наименование прибора
            device.append(delete_com(str(state.device.type)))  # 2 - Тип прибора
            device.append("1")  # 3 - Кол-во штук
            device.append(state.device.device_id)  # 4 - Зав. номер устройства
            device.append(state.device.birthplace)  # 5 - Месторождение
            devices.append(device)
            num += 1
            print(devices)
        return render(request, "devices/act_page3.html", {"devices": devices, "diag_res": diag_res, "repair_res": repair_res})


    def post_new_box(request):
        submitbutton = request.POST.get("submit")
        form = BoxChoiceForm(request.POST, request.FILES)
        print(form.data.get("box_id"))
        print(form.data.get("box_type"))
        print(form.data.get("arrive_time"))
        if form.is_valid():
            form_box_id = form.cleaned_data.get("box_id")
            form_box_type = form.cleaned_data.get("box_type")
            form_length = form.cleaned_data.get("length")
            form_width = form.cleaned_data.get("width")
            form_height = form.cleaned_data.get("height")
            form_weight = form.cleaned_data.get("weight")
            form_arrive_date = form.cleaned_data.get("time")
            print(form_arrive_date)

            berror = ""

            if (form_length is None) or (form_length == ''):
                form_length = 0
            if (form_width is None) or (form_width == ''):
                form_width = 0
            if (form_height is None) or (form_height == ''):
                form_height = 0
            if (form_weight is None) or (form_weight == ''):
                form_weight = 0

            print(form_box_id)
            print(form_box_id)

            if (form_box_id is None) or (form_box_id == ''):
                b = Box(box_type=form_box_type, package_length=form_length,
                        package_width=form_width,
                        package_height=form_height, package_weight=form_weight, arrive_date=form_arrive_date)
            else:
                b = Box(box_id=form_box_id, box_type=form_box_type, package_length=form_length,
                        package_width=form_width,
                        package_height=form_height, package_weight=form_weight, arrive_date=form_arrive_date)

            try:
                b.save()
            except:
                berror = "Ошибка: Не удалось сохранить ящик"

        box_choice_form = BoxChoiceForm()
        receipt_choice_form = ReceiptChoiceForm()
        box_choices() # обновляем список ящиков
        device_choice_form = DeviceChoiceForm()
        context = {'box_choice_form': box_choice_form,'receipt_choice_form': receipt_choice_form, 'device_choice_form': device_choice_form, 'berror': berror}

        return render(request, 'devices/new_device.html', context)

    def post_new_receipt(request):
        print("Что такое?")
        print(ReceiptChoiceForm(request.POST, request.FILES))

        form = ReceiptChoiceForm(request.POST, request.FILES)

        print(form.data.get("receipt_date"))
        if form.is_valid():
            form_receipt_date = form.cleaned_data.get("receipt_date")
            form_receipt_id = form.cleaned_data.get("receipt_id")
            rerror = ""

            has_image = True
            try:
                request.FILES['image']
            except:
                has_image = False

            if request.method == 'POST':
                if form_receipt_id != '':
                # Если дата квитанции НЕ пустая
                    if (form_receipt_date is not None) and (form_receipt_date != ''):
                        # Если изображение НЕ пустое
                        if has_image:
                            r = Receipt(receipt_id=form_receipt_id, receipt_date=form_receipt_date, image=request.FILES['image'])
                        # Если изображение пустое
                        else:
                            r = Receipt(receipt_id=form_receipt_id, receipt_date=form_receipt_date)
                    # сли дата квитанции пустая
                    else:
                        # Если изображение НЕ пустое
                        if has_image:
                            r = Receipt(receipt_id=form_receipt_id, image=request.FILES['image'])
                        # Если изображение пустое
                        else:
                            r = Receipt(receipt_id=form_receipt_id)
                    try:
                        r.save()
                    except:
                        rerror = "Ошибка: Не удалось сохранить квитанцию"

        box_choice_form = BoxChoiceForm()
        receipt_choice_form = ReceiptChoiceForm()
        box_choices() # обновляем список ящиков
        device_choice_form = DeviceChoiceForm()
        context = {'box_choice_form': box_choice_form,'receipt_choice_form': receipt_choice_form, 'device_choice_form': device_choice_form, 'rerror': rerror}

        return render(request, 'devices/new_device.html', context)

    def post_new_device(request):
        submitbutton = request.POST.get("submit")
        form = DeviceChoiceForm(request.POST, request.FILES)
        derror = ""
        berror = ""
        werror = ""
        sherror = ""
        if form.is_valid():

            form_device_id = form.cleaned_data.get("device_id")
            form_box = form.cleaned_data.get("box")
            form_arrive_date = form.cleaned_data.get("repair_date_begin")
            form_receipt = form.cleaned_data.get("receipt")
            form_shelf = form.cleaned_data.get("shelf") #Integer. Складывать в Statement
            form_photos = form.cleaned_data.get("photos") #Image
            form_name = form.cleaned_data.get("name")
            form_type = form.cleaned_data.get("type") #Varchar, multiplechocie
            form_denomination = form.cleaned_data.get("denomination")
            form_brand = form.cleaned_data.get("brand")
            form_birthplace = form.cleaned_data.get("birthplace")
            form_work = form.cleaned_data.get("work")
            form_doc = form.cleaned_data.get("doc")
            form_customer = form.cleaned_data.get("customer")
            derror = ""
            print("Тип")
            print(form_type)
            box_id = 0
            print(form_box)
            if form_box != '' and form_box:
                box_id = form_box[0][form_box[0].find('(') + 1: -1]
                box_id = int(box_id.split(")")[0])


            # Проверка на то, находится ли прибор в работе
            is_in_progress = False
            derror = ""
            sherror = ""
            werror = ""
            states = list(Statement.objects.filter(statement="В процессе"))
            dev_list = []
            for i in range(0, len(states)):
                dev_list.append(states[i].device.device_id)
            for i in dev_list:
                if str(form_device_id) == str(i):
                    is_in_progress = True
                    werror = "Ошибка: прибор с заводским номером: " + str(form_device_id) + " уже находится в работе."


            if not is_in_progress:
                #Проверка на занятость полки
                is_occupied = False
                sherror = ""
                states = list(Statement.objects.filter(statement = "В процессе"))
                shelfs_list = []
                for i in range(0, len(states)):
                    if states[i].shelf != 0:
                        shelfs_list.append(states[i].shelf)
                print(shelfs_list)
                for i in shelfs_list:
                    if form_shelf == i:
                        is_occupied = True
                        sherror = "Ошибка: ячейка №"+ str(form_shelf)+" уже занята."


                if not is_occupied:
                    try:
                        d = Device(device_id=form_device_id, name=form_name, type=form_type, denomination=form_denomination, brand=form_brand, birthplace=form_birthplace)
                        d.save()
                        device = Device.objects.get(device_id=form.cleaned_data['device_id'])
                        if box_id != 0:
                            box = Box.objects.get(id=box_id)
                        if form_receipt.exists():
                            if box_id != 0:
                                s = Statement(device=device, receipt=form_receipt[0], shelf=form_shelf, statement = "В процессе", box=box, customer=form_customer, work=form_work, doc=form_doc, repair_date_begin=form_arrive_date)
                            else:
                                s = Statement(device=device, receipt=form_receipt[0], shelf=form_shelf, statement="В процессе", customer=form_customer, work=form_work, doc=form_doc,repair_date_begin=form_arrive_date)
                        else:
                            if box_id != 0:
                                s = Statement(device=device, shelf=form_shelf, statement="В процессе", box=box, customer=form_customer, work=form_work, doc=form_doc, repair_date_begin=form_arrive_date)
                            else:
                                s = Statement(device=device, shelf=form_shelf, statement="В процессе", customer=form_customer, work=form_work, doc=form_doc, repair_date_begin=form_arrive_date)
                        s.save()
                        #for f in request.FILES.getlist('photos'):
                        #    di = DeviceImages(device=device, image=f)
                        #    di.save()
                        im = DeviceImages(device = device, image = form_photos)
                        im.save()

                    except:
                        derror = "Ошибка: Не удалось сохранить прибор"


        box_choice_form = BoxChoiceForm()
        receipt_choice_form = ReceiptChoiceForm()
        box_choices() # обновляем список ящиков
        device_choice_form = DeviceChoiceForm()
        context = {'box_choice_form': box_choice_form,'receipt_choice_form': receipt_choice_form, 'device_choice_form': device_choice_form, 'derror': derror, 'sherror': sherror, 'werror': werror}

        return render(request, 'devices/new_device.html', context)


    def post_new_diagnostics(request):
        form = DiagnosticsForm(request.POST)
        if form.is_valid():
            form_device_id = form.cleaned_data.get("device_id")
            form_diagnostics_result = form.cleaned_data.get("diagnostics_result")
            form_diagnostics_end_date = form.cleaned_data.get("diagnostics_end_date")
            print(form_diagnostics_result)
            derror = ""

            form_device_id_names = []
            print(form_device_id)
            for i in range(0, len(form_device_id)):
                buf_id = str(form_device_id[0]).split(")")[0]
                buf_id = int(buf_id.split(" ")[0])
                print(buf_id)
                form_device_id_names.append(buf_id)
            print(form_device_id_names)
            states = list(Statement.objects.filter(statement="В процессе"))

            customers = [] #Проверка на то, не ввёл ли пользователь приборы от разных заказчиков
            for dev_id in form_device_id_names:
                print(dev_id)
                statements = Statement.objects.filter(statement="В процессе", device=Device.objects.get(id=dev_id))
                for stat in statements:
                    if stat.customer not in customers:
                        customers.append(stat.customer)

            print(customers)
            print(len(customers))
            derror = ""
            if len(customers) >1:
                repair_choices()
                diag_choices()
                diagnostics_form = DiagnosticsForm()
                derror = "Ошибка: Нельзя сохранять данные по диагностикам для нескольких заказчиков за раз."
                return render(request, "devices/new_diagnostics.html", {'diagnostics_form': diagnostics_form,"derror": derror})
            else:
                dev_list = []
                for i in range(0, len(states)):
                    dev_list.append(states[i].device.id)
                print(dev_list)

                devices_ids = []
                DiagnosticsGroup().save()
                diagnostics_group = DiagnosticsGroup.objects.all().order_by('-id')[0]
                print("diagnostics group = ")
                print(diagnostics_group)
                for i in range(0, len(form_device_id_names)):
                    for j in dev_list:
                        print("form_device_id[i]")
                        print(form_device_id_names[i])
                        print(str(j))
                        if str(form_device_id_names[i]) == str(j):
                            device_id = ""
                            device_id = str(j)
                            devices_ids.append(device_id)
                            statement = Statement.objects.filter(device=device_id, statement="В процессе")[0]
                            save_statements(str(statement.statement_id), diagnostics_result=str(form_diagnostics_result), diagnostics_group = diagnostics_group, diagnostics_end_date = form_diagnostics_end_date)

                repair_choices()
                diag_choices()
                diagnostics_form = DiagnosticsForm()
                return render(request, "devices/new_diagnostics.html", {'diagnostics_form': diagnostics_form, "derror": derror})

    def post_new_repair(request):
        form = RepairForm(request.POST)
        if form.is_valid():
            form_device_id = form.cleaned_data.get("device_id")
            form_repair_result = form.cleaned_data.get("repair_result")
            form_repair_end_date = form.cleaned_data.get("repair_date_end")
            print("Print: ")
            print(form_repair_end_date)
            print(form_repair_result)
            rerror = ""

            form_device_id_names = []
            print(form_device_id)
            for i in range(0, len(form_device_id)):
                buf_id = str(form_device_id[0]).split(")")[0]
                buf_id = int(buf_id.split(" ")[0])
                print(buf_id)
                form_device_id_names.append(buf_id)
            print(form_device_id_names)
            states = list(Statement.objects.filter(statement="В процессе"))

            customers = []  # Проверка на то, не ввёл ли пользователь приборы от разных заказчиков
            for dev_id in form_device_id_names:
                print(dev_id)
                statements = Statement.objects.filter(statement="В процессе", device=Device.objects.get(id=dev_id))
                for stat in statements:
                    if stat.customer not in customers:
                        customers.append(stat.customer)

            print(customers)
            print(len(customers))
            rerror = ""
            if len(customers) > 1:
                repair_choices()
                diag_choices()
                repair_form = RepairForm()
                rerror = "Ошибка: Нельзя сохранять данные по диагностикам для нескольких заказчиков за раз."
                return render(request, "devices/new_diagnostics.html",
                              {'diagnostics_form': repair_form, "rerror": rerror})
            else:
                dev_list = []
                for i in range(0, len(states)):
                    dev_list.append(states[i].device.id)
                print(dev_list)

                devices_ids = []
                RepairGroup().save()
                repair_group = RepairGroup.objects.all().order_by('-id')[0]
                print("repair group = ")
                print(repair_group)
                for i in range(0, len(form_device_id_names)):
                    for j in dev_list:
                        print("form_device_id[i]")
                        print(form_device_id_names[i])
                        print(str(j))
                        if str(form_device_id_names[i]) == str(j):
                            device_id = ""
                            device_id = str(j)
                            devices_ids.append(device_id)
                            statement = Statement.objects.filter(device=device_id, statement="В процессе")[0]
                            print("Print 2: ")
                            print(form_repair_end_date)
                            save_statements(str(statement.statement_id),
                                            repair_result=str(form_repair_result),
                                            repair_group=repair_group,
                                            repair_date_end=form_repair_end_date)

                repair_choices()
                diag_choices()
                repair_form = RepairForm()
                return render(request, "devices/new_repair.html",
                              {'repair_form': repair_form, "rerror": rerror})

# Create your views here.
