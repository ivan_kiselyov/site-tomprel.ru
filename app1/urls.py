from django.urls import path
from . import views


urlpatterns = [
    path("", views.DeviceView.as_view(), name='main'),
    path('receipt', views.ManagerInterface.post_new_receipt, name='receipt'),
    path("transfer_act/<slug:act>/", views.ManagerInterface.get_act,  name='act_url'),
    # path("1/<slug:act>/", views.ManagerInterface.get_customers,  name='act_customer'),
    path("defects_act/<slug:act>/", views.ManagerInterface.get_act_defects,  name='act_url2'),
    path("repair_act/<slug:act>/", views.ManagerInterface.get_act_repair,  name='act_url3'),
    path('actlist', views.ManagerInterface.get_act_list, name='act_list'),
    path('actlist2', views.ManagerInterface.get_act_list_defects, name='act_list2'),
    path('actlist3', views.ManagerInterface.get_act_list_repair, name='act_list3'),
    path('deviceslist', views.DeviceView.get_devices_list, name='device_list'),
    #Поля заполнения БД менеджером
    path('boxarrival', views.ManagerInterface.get_new_device, name='box_arrival'),
    path('boxarrival2', views.ManagerInterface.post_new_box, name='box_arrival2'),

    path('boxarrival4', views.ManagerInterface.post_new_device, name='box_arrival4'),
    path('diagnostics', views.ManagerInterface.get_diagnostics, name='diagnostics'),
    path('diagnostics2', views.ManagerInterface.post_new_diagnostics, name='diagnostics2'),
    path('repair', views.ManagerInterface.get_new_repair, name='repair'),
    path('repair2', views.ManagerInterface.post_new_repair, name='repair2'),
]
