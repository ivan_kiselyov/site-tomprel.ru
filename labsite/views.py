from django.http import HttpResponseRedirect

def page(request):
    if request.user.is_authenticated:
        return HttpResponseRedirect('/main/')
    else:
        return HttpResponseRedirect('/accounts/login')